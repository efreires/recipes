package com.eladio.recipestest.utils;

/**
 * Created 07/05/2018.
 *
 * @author Eladio Freire
 */

public class Constants {

    // Set the duration of the splash screen
    public static final long SPLASH_SCREEN_DELAY = 3000;

    public static final int RESPONSE_UNKNOWN_ERROR = 0;
    public static final int RESPONSE_NO_DATA = 10;
    public static final int RESPONSE_CODE_200 = 200;


}
