package com.eladio.recipestest.ui.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.eladio.recipestest.R;
import com.eladio.recipestest.data.model.RecipeDetail;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created 07/05/2018.
 *
 * @author Eladio Freire
 */
public class DetailActivity extends BaseActivity {
    public static final String EXTRA_ITEM = "DetailActivity:extraItem";

    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.name)
    TextView tvNameRecipe;

    @BindView(R.id.description)
    TextView tvDescriptionRecipe;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this, this);

        RecipeDetail item = getIntent().getParcelableExtra(EXTRA_ITEM);
        Glide.with(image.getContext())
                .load(item.getThumbnail())
                .apply(new RequestOptions().error(R.drawable.placeholder2))
                .into(image);
        tvNameRecipe.setText(item.getIngredients());
        setSupportActionBar(toolbar);
        toolbar.setTitle(item.getTitle());
    }


}
