package com.eladio.recipestest.ui.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.eladio.recipestest.R;
import com.eladio.recipestest.data.model.RecipeDetail;
import com.eladio.recipestest.ui.activity.DetailActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created 07/05/2018.
 *
 * @author Eladio Freire
 */
public class RecipesAdapter extends RecyclerView.Adapter<RecipesAdapter.RecipeViewHolder> {


    private List<RecipeDetail> mRecipesDetails;
    private Activity mActivity;

    /**
     * Constructor class.
     *
     * @param recipeDetails list recipes details.
     */
    public RecipesAdapter(List<RecipeDetail> recipeDetails, Activity activity) {

        this.mRecipesDetails = recipeDetails;
        this.mActivity = activity;

    }

    @NonNull
    @Override
    public RecipeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recipes_layout, parent, false);
        ButterKnife.bind(this, v);

        return new RecipeViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecipeViewHolder holder, int position) {
        holder.bind(position);

    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return mRecipesDetails.size();
    }


    class RecipeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.tview_title)
        TextView recipeTitle;
        @BindView(R.id.tview_ingredients)
        TextView recipesIngredients;
        @BindView(R.id.tview_link)
        TextView recipeLink;
        @BindView(R.id.img_thum)
        ImageView recipeAvatar;
        private ItemClickListener clickListener;

        RecipeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

        }

        public void setClickListener(ItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
            clickListener.onClick(itemView, getLayoutPosition());

        }

        void bind(final int position) {

            recipeTitle.setText(mRecipesDetails.get(position).getTitle());
            recipesIngredients.setText(mRecipesDetails.get(position).getIngredients());
            recipeLink.setText(mRecipesDetails.get(position).getHref());

            Glide.with(recipeAvatar.getContext())
                    .load(mRecipesDetails.get(position).getThumbnail())
                    .apply(new RequestOptions().error(R.drawable.placeholder2))
                    .into(recipeAvatar);

            setClickListener(new ItemClickListener() {
                @Override
                public void onClick(View view, int position) {
                    Intent intent = new Intent(mActivity.getApplicationContext(), DetailActivity.class);
                    intent.putExtra(DetailActivity.EXTRA_ITEM, mRecipesDetails.get(position));

                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                            mActivity,
                            recipeAvatar,
                            mActivity.getApplicationContext().getString(R.string.image_transition));

                    ActivityCompat.startActivity(mActivity, intent, options.toBundle());
                }
            });

        }
    }

    /**
     * Get list recipes details data.
     *
     * @return list recipe details data.
     */
    public List<RecipeDetail> getData() {
        return mRecipesDetails;
    }

    /**
     * Set data recipes details.
     *
     * @param data recipe details list.
     */
    public void setData(List<RecipeDetail> data) {
        mRecipesDetails = data;
    }


}

