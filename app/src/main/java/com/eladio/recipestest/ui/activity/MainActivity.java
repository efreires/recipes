package com.eladio.recipestest.ui.activity;

import android.os.Bundle;

import com.eladio.recipestest.R;
import com.eladio.recipestest.ui.main.MainFragment;

/**
 * Created 07/05/2018.
 *
 * @author Eladio Freire
 */
public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        configureFragments(savedInstanceState);
    }


    /**
     * Configure fragment.
     *
     * @param savedInstanceState if null create fragment.
     */
    private void configureFragments(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.layout_info_main, MainFragment.newInstance(), MainFragment.TAG)
                    .commit();
            ;
        }
    }
}