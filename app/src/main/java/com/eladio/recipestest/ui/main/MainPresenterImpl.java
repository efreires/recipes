package com.eladio.recipestest.ui.main;

import android.content.Context;
import android.util.Log;

import com.eladio.recipestest.core.GetRecipesInteractor;
import com.eladio.recipestest.core.implementation.GetRecipesInteractorImpl;
import com.eladio.recipestest.data.model.RecipeDetail;

import java.util.List;

/**
 * Created 07/05/2018.
 *
 * @author Eladio Freire
 */
public class MainPresenterImpl implements MainPresenter {

    private String TAG = "MainViewPresenterImpl";
    private Context mContext;
    private MainView mView;
    private GetRecipesInteractor mGetRecipesInteractor;

    public MainPresenterImpl(Context context) {
        mContext = context;
        mGetRecipesInteractor = new GetRecipesInteractorImpl();

    }

    @Override
    public void onCreate(MainView view) {
        mView = view;
        System.out.print("");

    }

    /**
     * Get recipes response if the response is success update the data recipes.
     *
     * @param context context.
     */
    @Override
    public void getRecipes(Context context, String query, String ingredients, int pages) {
        showProgressDialog();
        mGetRecipesInteractor.getRecipes(new MainPresenter.RecipesListener() {
            @Override
            public void onSuccess(List<RecipeDetail> recipesResponse) {

                updateData(recipesResponse);
                hideProgressDialog();
            }

            @Override
            public void onFailure() {

                Log.i(TAG, "Error: Server Error");
                hideProgressDialog();
            }
        }, context, query, ingredients, pages);
    }


    /**
     * Update the data recipes in the recycler view.
     *
     * @param data recipes list.
     */
    public void updateData(List<RecipeDetail> data) {
        if (mView != null) {
            mView.onUpdateData(data);
        }
    }

    /**
     * Show progress Dialog.
     */
    private void showProgressDialog() {
        if (mView != null) {
            mView.onShowProgressDialog();
        }
    }

    /**
     * Hide progress dialog.
     */
    private void hideProgressDialog() {
        if (mView != null) {
            mView.onHideProgressDialog();
        }
    }

    @Override
    public void onDestroy() {
        mView = null;
    }
}
