package com.eladio.recipestest.ui.main;

import android.content.Context;

import com.eladio.recipestest.data.model.RecipeDetail;

import java.util.List;

/**
 * Created 07/05/2018.
 *
 * @author Eladio Freire
 */
public interface MainView {

    Context getContextPref();

    void onUpdateData(List<RecipeDetail> data);

    void onShowProgressDialog();

    void onHideProgressDialog();
}
