package com.eladio.recipestest.ui;


import android.support.v4.app.Fragment;

import com.eladio.recipestest.ui.activity.BaseActivity;


/**
 * Created 07/05/2018.
 *
 * @author Eladio Freire
 */

public class BaseFragment extends Fragment {


    /**
     * Gets the base activity
     *
     * @return activity
     */
    public BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }
}
