package com.eladio.recipestest.ui.main;

import android.content.Context;

import com.eladio.recipestest.data.model.RecipeDetail;

import java.util.List;

/**
 * Main presenter.
 *
 * Created 07/05/2018.
 *
 * @author Eladio Freire
 */
public interface MainPresenter {

    interface RecipesListener {

        void onSuccess(List<RecipeDetail> body);

        void onFailure();
    }

    void onCreate(MainView view);

    void getRecipes(final Context context, String query, String ingredients, int pages);

    void onDestroy();
}
