package com.eladio.recipestest.ui.adapter;

import android.view.View;

/**
 * Created 07/05/2018.
 *
 * @author Eladio Freire
 */

public interface ItemClickListener {
    /**
     * Click in item of recycler.
     *
     * @param view     view.
     * @param position position item detail.
     */
    void onClick(View view, int position);
}

