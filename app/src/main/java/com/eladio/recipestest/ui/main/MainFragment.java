package com.eladio.recipestest.ui.main;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.eladio.recipestest.R;
import com.eladio.recipestest.data.model.RecipeDetail;
import com.eladio.recipestest.ui.BaseFragment;
import com.eladio.recipestest.ui.adapter.RecipesAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;

/**
 * Created 07/05/2018.
 *
 * @author Eladio Freire
 */
public class MainFragment extends BaseFragment implements MainView {

    public static final String TAG = "InfoRecipesFragment";
    private MainPresenter mPresenter;
    private ProgressDialog mProgressDialog;

    @BindView(R.id.recycler)
    protected RecyclerView mRecyclerView;
    @BindView(R.id.editText)
    protected EditText searchEdit;


    /**
     * Default constructor.
     */
    public MainFragment() {

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.main_fragment, container, false);
        ButterKnife.bind(this, view);

        return view;

    }

    /**
     * MainFragment instance.
     *
     * @return instance.
     */
    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPresenter = new MainPresenterImpl(getContextPref());
        mPresenter.onCreate(this);


    }


    /**
     * Call Search recipes. Get list of recipes data.
     */
    @OnTextChanged(R.id.editText)
    void searchRecipes(CharSequence letterSequence, int start, int count, int after) {
        String query = letterSequence.toString();
        mPresenter.getRecipes(getContext(), query, "", 1);

    }

    @Override
    public Context getContextPref() {
        return getContext();
    }


    /**
     * Set data in the recycler view.
     *
     * @param data list recipes data.
     */
    @Override
    public void onUpdateData(List<RecipeDetail> data) {

        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(llm);
        mRecyclerView.setVisibility(View.VISIBLE);
        if (mRecyclerView.getAdapter() != null) {

            RecipesAdapter adapter = (RecipesAdapter) mRecyclerView.getAdapter();
            adapter.setData(data);
            adapter.notifyDataSetChanged();


        } else {
            RecipesAdapter adapter = new RecipesAdapter(data, getActivity());
            mRecyclerView.setAdapter(adapter);
        }
    }


    /**
     * Show dialog.
     */
    @Override
    public void onShowProgressDialog() {


        if (mProgressDialog == null) {

            mProgressDialog = new ProgressDialog(getActivity(), R.style.AlertDialogStyle);
            mProgressDialog.setMessage(getString(R.string.searching));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
        }

        mProgressDialog.show();
    }

    /**
     * Hide Dialog.
     */
    @Override
    public void onHideProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {

        super.onResume();
    }


}
