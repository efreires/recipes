package com.eladio.recipestest.network;

import com.eladio.recipestest.data.model.Recipes;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created 07/05/2018.
 *
 * @author Eladio Freire
 */
public interface APIServices {


    /**
     * Webservice for  data sending
     *
     * @return Response
     */

    @GET("./")
    Call<Recipes> getRecipes(@Query("i") String ingredient,
                             @Query("q") String query,
                             @Query("p") int page);

}



