package com.eladio.recipestest.network;

import android.content.Context;
import android.util.Log;

import com.eladio.recipestest.BuildConfig;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created 07/05/2018.
 *
 * @author Eladio Freire
 */

public class ApiServicesImplementation implements APINetworkingInterface {

    @Override
    public <S> S getRetrofitConnection(Class<S> serviceClass, Context context) {

        Log.d("prueba", "Creating connection " + serviceClass);
        Retrofit.Builder rbuilder = new Retrofit.Builder()
                .baseUrl(BuildConfig.ENDPOINT_RECIPES)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create());

        OkHttpClient.Builder httpClient = new OkHttpClient().newBuilder();

        httpClient.hostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(final String s, final SSLSession sslSession) {
                return true;
            }
        });

        httpClient.interceptors().clear();


        HttpLoggingInterceptor logInterceptor = new HttpLoggingInterceptor();
        logInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        httpClient.addInterceptor(logInterceptor);

        rbuilder.client(httpClient.build());

        return rbuilder.build().create(serviceClass);

    }
}
