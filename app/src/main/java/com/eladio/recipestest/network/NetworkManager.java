package com.eladio.recipestest.network;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.eladio.recipestest.data.loader.DataCallback;
import com.eladio.recipestest.data.model.RecipeDetail;
import com.eladio.recipestest.data.model.Recipes;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.eladio.recipestest.utils.Constants.RESPONSE_CODE_200;
import static com.eladio.recipestest.utils.Constants.RESPONSE_NO_DATA;
import static com.eladio.recipestest.utils.Constants.RESPONSE_UNKNOWN_ERROR;


/**
 * Created 07/05/2018.
 *
 * @author Eladio Freire
 */

public class NetworkManager {

    private DataCallback<List<RecipeDetail>> mCallback;
    private List<RecipeDetail> mResultList;

    private static NetworkManager mInstance = null;
    private APINetworkingInterface mServicesInterface;

    private Context mContext;

    /**
     * Singleton.Network Manager Instance.
     *
     * @return instance MXNetworkManager.
     */
    public static NetworkManager getInstance(Context ctx) {
        if (mInstance == null) {
            mInstance = new NetworkManager();
        }

        mInstance.mServicesInterface = new ApiServicesImplementation();
        /** Inits the non-static attributes with the passed context (non-static) */
        mInstance.mContext = ctx;

        return mInstance;
    }

    /**
     * Get different recipes with the Api Services.
     *
     * @param callback callback response.
     */
    public void getRecipesApi(final DataCallback<List<RecipeDetail>> callback, String ingredient, String query, int page) {

        setCallback(callback);
        APIServices apiServices = mInstance.mServicesInterface.getRetrofitConnection(APIServices.class, mInstance.mContext);
        Call<Recipes> call = apiServices.getRecipes(ingredient, query, page);

        call.enqueue(new Callback<Recipes>() {
            @Override
            public void onResponse(@NonNull Call<Recipes> call, @NonNull Response<Recipes> response) {

                if (response != null && response.code() == RESPONSE_CODE_200) {

                    Recipes result = response.body();
                    if (result != null && result.getResults().size() > 0) {

                        if (result.getResults().get(0) != null) {

                            if (result.getResults().size() > 0) {
                                mResultList = result.getResults();
                                if (mResultList != null && mResultList.size() > 0)
                                    Log.d("Values response: ", mResultList.toString());
                            } else {
                                mResultList = new ArrayList<>();
                            }
                            response(mResultList);

                        } else {
                            Log.d("Recipes", "No value for recipes available");
                            failure(RESPONSE_NO_DATA);

                        }
                    } else {
                        Log.d("results", "no results");
                        failure(RESPONSE_UNKNOWN_ERROR);

                    }

                } else {
                    failure(response.code());
                }
            }

            @Override
            public void onFailure(@NonNull Call<Recipes> call, @NonNull Throwable t) {
                failure(RESPONSE_UNKNOWN_ERROR);
            }
        });
    }


    /**
     * User request response.
     *
     * @param result response data
     */

    private void response(List<RecipeDetail> result) {
        if (mCallback != null) {
            mCallback.onResponse(result);
        }
    }

    /**
     * Fail response.
     *
     * @param error id.
     */
    private void failure(int error) {
        if (mCallback != null) {
            mCallback.onFail(error);
        }
    }

    /**
     * Set callback.
     *
     * @param callback callback.
     */
    public void setCallback(DataCallback<List<RecipeDetail>> callback) {

        mCallback = callback;
    }

    /**
     * Remove callback.
     */
    public void removeCallBack() {
        mCallback = null;
    }

}
