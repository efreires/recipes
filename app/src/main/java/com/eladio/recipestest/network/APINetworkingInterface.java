package com.eladio.recipestest.network;

import android.content.Context;

/**
 * Api networking interface.
 * Created 07/05/2018.
 *
 * @author Eladio Freire
 */

public interface APINetworkingInterface {

    <S> S getRetrofitConnection(Class<S> serviceClass, Context context);

}
