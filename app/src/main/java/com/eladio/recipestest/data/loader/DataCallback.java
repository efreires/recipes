package com.eladio.recipestest.data.loader;


/**
 * Created 07/05/2018.
 *
 * @param <T> template
 * @author Eladio Freire
 * Return the webservice response.
 */
public interface DataCallback<T> {

    void onResponse(T t);

    void onFail(int message);
}