package com.eladio.recipestest.data.loader;

import android.content.Context;

import com.eladio.recipestest.data.model.RecipeDetail;
import com.eladio.recipestest.network.NetworkManager;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created 07/05/2018.
 *
 * @author Eladio Freire
 */

public class RecipesLoader {

    private ExecutorService mThreadExecutor;
    private DataCallback<List<RecipeDetail>> mCallback;

    /**
     * Constructor with executor.
     */
    public RecipesLoader() {
        mThreadExecutor = Executors.newSingleThreadExecutor();

    }

    /**
     * Set callback recipe info.
     *
     * @param callback callback.
     */
    public void setCallback(DataCallback<List<RecipeDetail>> callback) {
        mCallback = callback;
    }

    /**
     * Remove callback.
     */
    public void removeCallBack() {
        mCallback = null;
    }

    /**
     * Load the webService to get the recipes info.
     *
     * @param context context app.
     */
    public void load(final Context context, final String ingredient, final String query, final int page) {

        mThreadExecutor.execute(new Runnable() {
            @Override
            public void run() {
                loadData(context, ingredient, query, page);
            }
        });
    }

    /**
     * Load the webService to get the info recipes.
     *
     * @param context context app
     */
    private void loadData(final Context context, String ingredient, String query, int page) {

        NetworkManager.getInstance(context).getRecipesApi(mCallback, ingredient, query, page);

    }


}
