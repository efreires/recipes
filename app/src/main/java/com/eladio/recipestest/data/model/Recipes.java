package com.eladio.recipestest.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Recipes.
 *
 * @author eladiosuarez
 * Created 07/05/2018.
 */
public class Recipes {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("version")
    @Expose
    private float version;
    @SerializedName("href")
    @Expose
    private String href;
    @SerializedName("results")
    @Expose
    private List<RecipeDetail> results = null;

    public Recipes(String title, float version, String href, List<RecipeDetail> results) {
        this.title = title;
        this.version = version;
        this.href = href;
        this.results = results;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getVersion() {
        return version;
    }

    public void setVersion(float version) {
        this.version = version;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public List<RecipeDetail> getResults() {
        return results;
    }

    public void setResults(List<RecipeDetail> results) {
        this.results = results;
    }

    @Override
    public String toString() {
        return "Recipes{" +
                "title='" + title + '\'' +
                ", version=" + version +
                ", href='" + href + '\'' +
                ", results=" + results +
                '}';
    }
}
