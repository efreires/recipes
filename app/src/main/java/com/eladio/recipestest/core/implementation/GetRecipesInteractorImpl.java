package com.eladio.recipestest.core.implementation;

import android.content.Context;

import com.eladio.recipestest.core.GetRecipesInteractor;
import com.eladio.recipestest.data.loader.DataCallback;
import com.eladio.recipestest.data.loader.RecipesLoader;
import com.eladio.recipestest.data.model.RecipeDetail;
import com.eladio.recipestest.ui.main.MainPresenter;

import java.util.List;

/**
 * Created 07/05/2018.
 *
 * @author Eladio Freire
 * <p>
 * Interactor Class.
 * Launches the loader that will get the recipes data.
 */

public class GetRecipesInteractorImpl implements GetRecipesInteractor {

    @Override
    public void getRecipes(final MainPresenter.RecipesListener recipesListener, Context context, String query, String ingredients, int pages) {
        final RecipesLoader loader = new RecipesLoader();

        loader.setCallback(new DataCallback<List<RecipeDetail>>() {
            @Override
            public void onResponse(final List<RecipeDetail> recipesResponse) {
                loader.removeCallBack();
                recipesListener.onSuccess(recipesResponse);
            }

            @Override
            public void onFail(int message) {
                recipesListener.onFailure();
            }
        });
        loader.load(context, ingredients, query, pages);
    }


}
