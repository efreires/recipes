package com.eladio.recipestest.core;


import android.content.Context;

import com.eladio.recipestest.ui.main.MainPresenter;

/**
 * Created 07/05/2018.
 *
 * @author Eladio Freire
 */

public interface GetRecipesInteractor {

    void getRecipes(MainPresenter.RecipesListener recipesListener, Context context, String query, String ingredients, int pages);

}
